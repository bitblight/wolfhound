"""
wolfhound.targets

"""

import copy
import logging

from .bloodhound import load_data_model
from .model import Target

__all__ = [
    'collate_targets'
]

def get_member_group_users(model, group_sid):

    # These are probably SIDs for local groups that aren't in the Bloodhound
    # dataset
    try:
        group = model['groups'][group_sid]
    except KeyError as e:
        logging.debug(f'group sid {group_sid} was not in the index')
        return set()

    member_user_sids = copy.copy(group['member_users'])

    for member_group_sid in group['member_groups']:

        member_user_sids.update(
            get_member_group_users(
                model,
                member_group_sid
            )
        )

    return member_user_sids

def collate_targets(model):

    targets = set()

    for computer_sid, computer in model['computers'].items():

        for user_sid in computer['local_admins']['users']:

            targets.add(
                Target(
                    model['users'][user_sid]['domain'],
                    model['users'][user_sid]['username'],
                    computer['computer_name']
                )
            )

        for group_sid in computer['local_admins']['groups']:

            for member_user_sid in get_member_group_users(model, group_sid):
                targets.add(
                    Target(
                        model['users'][member_user_sid]['domain'],
                        model['users'][member_user_sid]['username'],
                        computer['computer_name']
                    )
                )

    return targets

