#!/usr/bin/env python3
"""
wolfhound

"""

# TODO: Add computer account support
# TODO: Add NetBIOS support if it doesn't quite work on all setups
# TODO: Add NetBIOS user domain query support

import argparse
import logging

from .bloodhound import load_data_model
from .ntlmrelayx import print_ntlmrelayx_urls
from .targets import collate_targets

def main():

    parser = argparse.ArgumentParser(
        prog='wolfhound',
        description=(
            "Parses BloodHound zip archives to produce an ntlmrelayx list "
            "targeting domain users with local administrator rights at their "
            "respective computers.\n"
            "\n"
            "ntlmrelayx matches user workgroups by their NetBIOS name, while "
            "BloodHound uses FQDNs. To ensure accurate matching you should pass "
            "the -d/--domain switch with the NetBIOS name of the domain you are "
            "targeting. Automatic NetBIOS domain name resolution is not presently "
            "supported.\n"
            "\n"
            "Additionally, BloodHound datasets refer to computers by hostname and "
            "don't contain cached network addresses. ntlmrelayx, on the other "
            "hand, appears to match incoming connections with the target list "
            "by network address. To ensure smooth operation, hostnames should "
            "be resolved before use. This can be done automatically by "
            "passing the -r/--resolve switch if you are connected to a "
            "nameserver that can resolve hostnames for the target domain(s).\n"
        ),
        epilog=(
            "Send bug reports and suggestions to "
            "<shaun.wheelhouse@themissinglink.com.au>"
        ),
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version='%(prog)s 0.9.0'
    )

    parser.add_argument(
        '-r', '--resolve-names',
        action='store_true',
        help=(
            'resolve hostnames to network addresses'
        )
    )

    parser.add_argument(
        '-D', '--debug',
        action='store_true',
        help=(
            'print debugging information'
        )
    )

    parser.add_argument(
        '-d', '--domain',
        metavar='NETBIOS_DOMAIN',
        help=(
            'a NetBIOS domain to substitute for user FQDNs'
        )
    )

    parser.add_argument(
        'bloodhound_data',
        nargs='+',
        help=(
            'one or more BloodHound zip archives'
        )
    )

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    targets = set()
    for archive_path in args.bloodhound_data:
        model = load_data_model(archive_path)
        targets.update(collate_targets(model))

    targets = list(targets)
    targets.sort()

    print_ntlmrelayx_urls(targets)

if __name__ == '__main__':

    main()

