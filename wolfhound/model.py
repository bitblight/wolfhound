"""
wolfhound.model

"""

from collections import namedtuple

Target = namedtuple('Target', [
    'domain',
    'username',
    'computer'
])

