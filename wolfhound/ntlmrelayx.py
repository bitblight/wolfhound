"""
wolfhound.ntlmrelayx

"""

import logging
import socket
import sys

from .model import Target

__all__ = [
    'print_ntlmrelayx_urls'
]

def strip_domain_qualifier(identity):

    return identity.split('@', maxsplit=1)[0]

def transform_target_for_ntlmrelayx(target, fallback_domain=None,
        name_cache={}, resolve_names=False):
    """Transforms a Target record from Bloodhound-style names to
    ntlmrelayx-style names.

    """

    domain, computer = target.domain, target.computer

    if resolve_names:

        if fallback_domain is not None:
            domain = fallback_domain

        # ntlmrelayx thinks in terms of IPs, so to avoid wasteful reflective
        # relays the names can be resolved and substituted with IPs
        try:
            computer = socket.gethostbyname(target.computer)
            name_cache[target.computer] = computer
        except:
            logging.warning(
                f'name lookup for computer {target.computer} failed'
            )

    elif fallback_domain is not None:
        domain = fallback_domain

    return Target(
        domain,
        # ntlmrelayx expects the user without the domain qualifier.
        strip_domain_qualifier(target.username),
        computer
    )

def print_ntlmrelayx_urls(targets, f=sys.stdout, fallback_domain=None,
        resolve_names=False):

    name_cache = {}

    targets = map(
        lambda target: transform_target_for_ntlmrelayx(
            target,
            fallback_domain=fallback_domain,
            name_cache=name_cache,
            resolve_names=resolve_names
        ),
        targets
    )

    for target in targets:
        print(
            f'smb://{target.domain}\\{target.username}@{target.computer}',
            file=f
        )

