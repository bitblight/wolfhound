"""
wolfhound.bloodhound

"""

import json
import zipfile

from .model import Target

__all__ = [
    'load_data_model'
]

def load_datasets(archive_path):
    """Load the JSON datasets from a BloodHound zip archive.

    The datasets are loaded into memory all at once. Large datasets may
    consume a noticeable amount of memory.

    Returns a dictionary of datasets:

    {
        'computers': {},
        'groups': {},
        'users': {}
    }

    """

    datasets = {}

    with zipfile.ZipFile(archive_path) as archive:

        index = {}
        for filename in archive.namelist():
            for suffix, key in [
                        ('_computers.json', 'computers'),
                        ('_groups.json', 'groups'),
                        ('_users.json', 'users')
                    ]:
                if filename.endswith(suffix):
                    index[key] = filename
                    continue

        datasets = {}
        for key, filename in index.items():
            with archive.open(filename) as dataset:
                datasets[key] = json.load(dataset)

    return datasets

def build_computer_data_model(computer_dataset):

    computer_model = {}

    for computer in computer_dataset['computers']:

        try:
            sid = computer['Properties']['objectid']
        except KeyError:
            continue

        computer_record = {
            'domain': computer['Properties']['domain'],
            'computer_name': computer['Properties']['name'],
            'local_admins': {
                'users': set(),
                'groups': set()
            }
        }

        for local_admin in computer['LocalAdmins']:

            if local_admin['MemberType'] == 'User':
                computer_record['local_admins']['users'].add(
                    local_admin['MemberId']
                )

            elif local_admin['MemberType'] == 'Group':
                computer_record['local_admins']['groups'].add(
                    local_admin['MemberId']
                )

        computer_model[sid] = computer_record

    return computer_model

def build_group_data_model(group_dataset):

    group_model = {}

    for group in group_dataset['groups']:

        try:
            sid = group['Properties']['objectid']
        except KeyError:
            continue

        group_record = {
            'domain': group['Properties']['domain'],
            'member_users': set(),
            'member_groups': set()
        }

        for group_member in group['Members']:

            if group_member['MemberType'] == 'User':
                group_record['member_users'].add(
                    group_member['MemberId']
                )

            elif group_member['MemberType'] == 'Group':
                group_record['member_groups'].add(
                    group_member['MemberId']
                )

        group_model[sid] = group_record

    return group_model

def build_user_data_model(user_dataset):

    user_model = {}

    for user in user_dataset['users']:

        try:
            sid = user['Properties']['objectid']
        except KeyError:
            continue

        user_record = {
            'domain': user['Properties']['domain'],
            'username': user['Properties']['name']
        }

        user_model[sid] = user_record

    return user_model

def build_data_model(datasets):

    # {
    #   'users': {
    #     '{sid}': {
    #       'domain': domain,
    #       'username': username
    #     }
    #   },
    #   'groups': {
    #     '{sid}': {
    #       'domain': domain,
    #       'member_users': set(sids),
    #       'member_groups': set(sids)
    #     }
    #   },
    #   'computers': {
    #     '{computer_sid}': {
    #       'domain': domain,
    #       'computer_name': computer_name',
    #       'local_admins': {
    #         'users': set(sids),
    #         'groups': set(sids)
    #       }
    #     }
    #   }
    # }

    return {
        'computers': build_computer_data_model(datasets['computers']),
        'groups': build_group_data_model(datasets['groups']),
        'users': build_user_data_model(datasets['users'])
    }

def load_data_model(archive_path):

    return build_data_model(load_datasets(archive_path))

