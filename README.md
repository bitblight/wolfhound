# wolfhound

**wolfhound is unlikely to be compatible with current archive schemas. I don't presently have plans to update; I have another way of getting this information. Treat this tool as archived.**

A tool for generating ntlmrelayx target files targeting local administrators from BloodHound datasets.

## Installation

```
$ cd wolfhound/
$ python -m pip install .
```

## Usage

```
$ wolfhound --help
```
